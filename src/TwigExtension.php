<?php

namespace Drupal\entity_twig;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldItemListInterface;

class TwigExtension extends \Twig_Extension {

  /**
   * {@inheritDoc}
   */
  public function getFunctions() {
    return [
      new \Twig_SimpleFunction('entity_load', [$this, 'entityLoad']),
    ];
  }

  /**
   * Load an entity.
   *
   * @param $entityTypeId
   * @param $entityId
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function entityLoad($entityTypeId, $entityId) {
    try {
      $controller = \Drupal::entityTypeManager()->getStorage($entityTypeId);
    } catch (PluginNotFoundException $e) {
      return NULL;
    }
    $entity = $controller->load($entityId);
    return $entity;
  }

  /**
   * {@inheritDoc}
   */
  public function getFilters() {
    return [
      new \Twig_SimpleFilter('entity_url', [$this, 'entityToUrl']),
      new \Twig_SimpleFilter('entity_access', [$this, 'entityAccess']),
      new \Twig_SimpleFilter('field_access', [$this, 'fieldAccess']),
    ];
  }

  /**
   * Wraps EntityInterface::toUrl.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param string $rel
   * @param array $options
   *
   * @return \Drupal\Core\Url
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function entityToUrl(EntityInterface $entity, $rel = 'canonical', $options = []) {
    return $entity->toUrl($rel, $options);
  }

  /**
   * Wraps EntityInterface::access.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param string $operation
   * @param \Drupal\Core\Session\AccountInterface $account
   *
   * @return bool
   */
  public function entityAccess(EntityInterface $entity, $operation, $account = NULL) {
    return $entity->access($operation, $account);
  }

  /**
   * Wraps FieldItemListInterface::access.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $fieldItemList
   * @param string $operation
   * @param \Drupal\Core\Session\AccountInterface $account
   *
   * @return bool
   */
  public function fieldAccess(FieldItemListInterface $fieldItemList, $operation, $account = NULL) {
    return $fieldItemList->access($operation, $account);
  }

}
